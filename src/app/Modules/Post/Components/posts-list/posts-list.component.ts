import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { PostService } from '../../Services/post.service';
import { Post } from '../../Models/post.model';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css'],
})
export class PostsListComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['id', 'userId', 'title'];

  private _table : MatTable<any>;
  @ViewChild('table')
  set table(value:MatTable<any>){
    this._table = value;
    this._table.renderRows()
  }
  get table():MatTable<any>{
    return this._table
  }

  posts: Post[] = [];

  temp: Post[] = [];

  searchedText:string = ''

  isBusy: boolean = false;

  constructor(private postService: PostService, private router:Router, private route : ActivatedRoute) {}

  ngOnInit(): void {
    this.fillPosts();
  }

  fillPosts() {
    this.isBusy = true;
    this.postService
      .posts$
      .pipe(takeUntil(this.postService.unSubscriber))
      .subscribe(
        (lst) => {
          console.log(lst)
          this.isBusy = false;
          this.posts.push(...lst);
          this.temp.push(...lst);
          if(this.table){
            this.table.renderRows();
          }
        },
        (error) => {
          this.isBusy = false;
        }
      );
  }

  ngOnDestroy() {
    this.postService.unSubscriber.next();
    this.postService.unSubscriber.complete();
  }

  navigateToEditListItem(post: Post) {
    console.log(post);

    this.router.navigate(['/post', {id: post.id}], {relativeTo:this.route});

  }

  applyFilter() {
    if (this.searchedText) {
      let tmpList: Post[] = [];
      tmpList = this.temp.filter((res) => {
        return String(res.userId)
          .toLowerCase()
          .trim()
          .match(this.searchedText.toLowerCase());
      });

      if (tmpList) {
        this.posts = tmpList;
        this.table.renderRows();
      }
    } else {
      this.posts = this.temp;
      this.table.renderRows();
    }
  }
}
