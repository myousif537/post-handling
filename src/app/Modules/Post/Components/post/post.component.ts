import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../../Models/post.model';
import { PostService } from '../../Services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
})
export class PostComponent implements OnInit, OnDestroy {
  params$;
  posts: Post[] = [];

  post: Post = new Post();

  constructor(private route: ActivatedRoute, private postService: PostService) {
    this.params$ = this.route.params.subscribe((param) => {
      let id = parseInt(param['id']);
      if (id !== undefined && id !== 0) this.fillPost(id);
    });
  }

  ngOnInit(): void {}

  fillPost(id) {
    this.postService.posts$.subscribe((lst) => {
      this.posts = lst;
      this.post = this.posts.find((post) => post.id === id);
      console.log(this.post);
      console.log(this.posts);
    });

  }

  savePost(){
    //this function supports save function which is means that will save and update in the same function via different apiUrls
    if(this.post.id){
      console.log('update')
    }else {
      console.log("save")
    }
  }

  ngOnDestroy() {
    this.params$.unsubscribe();
  }
}
