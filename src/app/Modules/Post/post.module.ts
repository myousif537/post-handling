import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../../material.module'
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { PostRoutingModule, routedComponents} from './post-routing.module';



import {
PostService

} from './index';

@NgModule({
  declarations: [
    routedComponents,



  ],
  imports: [
    PostRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    MaterialModule,





  ],
  providers: [PostService],
  entryComponents: []
})
export class PostModule { }
