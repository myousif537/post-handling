import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { map, timeout, retry, catchError } from 'rxjs/operators';
import { Post } from '../Models/post.model';

@Injectable({
  providedIn: 'root',
})
export class PostService {


  private postsCash = new BehaviorSubject([]);
  public posts$ : Observable<any> = this.postsCash.asObservable();






  constructor(private http: HttpClient) {
    this.getPosts().subscribe();
  }



  public unSubscriber: Subject<any> = new Subject();

  getPosts(): Observable<any> {
    return this.http.get('https://jsonplaceholder.typicode.com/posts').pipe(
      timeout(60000),
      map((res: Post[]) => {
        if (!res && !res.length) return;
        console.log(res);

        this.postsCash.next(res)
        return res;
      }),
      retry(0)
    );
  }
}
