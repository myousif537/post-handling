import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostComponent, PostsListComponent } from './index';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'list',
        component: PostsListComponent,
      },

      {
        path: '',
        children: [
          {
            path: '',
            component: PostComponent,
          },

          {
            path: 'id',
            component: PostComponent,
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostRoutingModule {}

export const routedComponents = [PostComponent, PostsListComponent];
